const formulario = document.getElementById('form');
const inputs = document.querySelectorAll('#form input');

const expresiones = {
    cedula: /^\d{10}$/, // Formato numérico de 10 dígitos
    nombre: /^[a-zA-ZÀ-ÿ\s]{1,50}$/, //Formato Texto de al menos 50 digitos.
    direccion:/^([a-zA-Z0-9_-\s]){1,100}$/, //Formato Alfanumerico de al menos 100 digitos.
    telefono: /^\d{10}$/, //Formato numérico de 10 dígitos
    correo: /\S+@\S+.\S+/, // Formato correcto del correo.
}

const validarFormulario = (e) => {
    switch (e.target.id) {

        case "cedula":
            if (expresiones.cedula.test(e.target.value)) {
                document.getElementById("mensaje1-error").innerHTML = "Cedula Valida";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje1-error").innerHTML = "Cedula no Valida";
            }
            break;
            
            case "nombre":
            if (expresiones.nombre.test(e.target.value)) {
                document.getElementById("mensaje2-error").innerHTML = "Nombre Valido";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje2-error").innerHTML = "Nombre no Valido";
            }
            break;

            case "direccion":
            if (expresiones.direccion.test(e.target.value)) {
                document.getElementById("mensaje3-error").innerHTML = "Direccion Valida";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje3-error").innerHTML = "Direccion no Valida";
            }
            break;

            case "telefono":
            if (expresiones.telefono.test(e.target.value)) {
                document.getElementById("mensaje4-error").innerHTML = "Telefono Valido";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje4-error").innerHTML = "Telefono no Valido";
            }
            break;

            case "correo":
            if (expresiones.correo.test(e.target.value)) {
                document.getElementById("mensaje5-error").innerHTML = "Correo Valido";
                document.getElementById("mensaje-error").innerHTML = "";
            } else {
                document.getElementById("mensaje5-error").innerHTML = "Correo no Valido";
            }
            break;

        }
    }

    inputs.forEach((input) => {
        input.addEventListener('keyup', validarFormulario);
        input.addEventListener('blur', validarFormulario);
    });
    
    formulario.addEventListener('submit', (e) => {
        e.preventDefault();
        var cedula = document.getElementById('cedula').value;
        var nombre = document.getElementById('nombre').value;
        var direccion = document.getElementById('direccion').value;
        var telefono = document.getElementById('telefono').value;
        var correo = document.getElementById('correo').value;

        if (!cedula.trim("") || !nombre.trim("")   || !direccion.trim("")
                             || !telefono.trim("") || !correo.trim("")) {
        document.getElementById("mensaje-error").innerHTML = "Ingrese todos los campos";
    } else {
        
        location.reload();
        document.form1.submit();
        
    }
});